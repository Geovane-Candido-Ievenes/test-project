var regexp = /a.db/g;
var string = '788addbffffagdb';

console.log(regexp.exec(string)); //o método exec retorna num objeto, o index da primeira ocorrência que corresponde a expressão regular, aqui no caso, 3
// aqui index = 3

console.log(string.match(regexp));

var result = regexp.exec(string).input;
var index = regexp.exec(string).index; // Mas na segunda vez que executada, ele voltará de onde parou, e procurará a próxima ocorrência, que no caso, seria no index 11
//e aqui index = 11

console.log(result.substr(index , string.split('').length));

////////////////////////////////////////////////////////

var regexp2 = /dpa-(\d{3})-(dsa)/,
	string2 = 'dpa-385-dsa';

string2.replace(regexp2 , 'O número retirado dessa string é: $1, e a teceira trinca de números é: $2');
//RETORNARÁ UMA STRING ALTERADA DE ACORDO COM A EXPRESSÃO REGULAR INSERIDA, CASO NÃO, RETORNA A STRING DA MESMA MANEIRQA QUE ESTIVER

////////////////////////////////////////////////////////

var regexp3 = /a/gi,
	string3 = 'Abc abc' //O i indica retirta a necessidade do CaseSensitive

console.log(string3.replace(regexp3 , '2')); //Todos os 'As' serão substituídos por 2

////////////////////////////////////////////////////////

var regexp4 = /\d{3}/,
	string4 = 'dap-345-pad';

console.log(string4.search(regexp4));
console.log(string4.substr(string4.search(regexp4) , string4.split('').length)) //TESTE, EXIBIDO DESDE A OCORRÊNCIA ATÉ O FINAL DA STRING

////////////////////////////////////////////////////////

//Assim como dito antes, já que o método exec retorna as ocorrências uma a uma conforme o camando é executado
//é possível se aproveitar disso fara fazer loops

var regexp5  = /(d.p)/g,
	string5 = 'jkfdapjjddgp',
	match;

while(match = regexp5.exec(string5)){ //SE FOR FALSO, RETORNARÁ NULL
	console.log(`ocorrência: ${match[0]}, index: ${match.index}`);
}

///////////////////////////////////////////////////////////////////

//DESAFIO

var formatdate = function(date){
	return date.replace(/(\d{2})\/(\d{2})\/(\d{4})/ , '$3/$2/$1');
}