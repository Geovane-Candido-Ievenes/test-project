const factorial = function(n){
	if(n===0){
		return 1;
	}else{
		return n * factorial(n-1); //Para cada chamada, ele adicionará para a Call Stack
		//Mas tome  cuidado !! se você fizer chamadas demais,
		//seu programa irá travar
	}
}

console.log(factorial(4));

//é possível dar um outro nome para a funcção quando utilizamos uma adicionamos uma função a uma variável
//utiliozando uma "expressão de função"

const factorial2 = function fac(n){
	if(n===0){
		return 1;
	}else{
		return n * fac(n-1);
	}
}

//Você pode usar isso como uma boa prática para indicar que naquela
//função haverá recursividade ;)