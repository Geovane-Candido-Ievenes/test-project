class Pencil{
	constructor(color, status){
		this.color = color;
		this.status = status;
	}

	write(message){
		document.write(message + '<br><br>');
	}
}

class Rascunhadeira extends Pencil{
	constructor(color, status, inkcolor){
		super(color, status)
			this.status = status * 2;
			this.inkcolor = inkcolor;
				this.rascunho = 0;
	}

	rascunhar(message){
		if(message!==undefined){
			this.rascunho++;
			this.write(message);
			document.write('--Rascunho ' + this.rascunho + ' Finalizado--<br><br>')
		}else{
			console.log('No momento, o status do seu lapis continua o mesmo');
		}
	}
}

rascunhadeira1 = new Rascunhadeira('amarelo', 15, 'azul');